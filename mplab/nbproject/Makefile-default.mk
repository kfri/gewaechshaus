#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/mplab.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/mplab.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=src/Main_Test.c src/edaPIC33ADC.c src/edaPIC33BlinkLed.c src/edaPIC33DS12B20.c src/edaPIC33Hardware.c src/edaPIC33LCD.c src/edaPIC33LCD5110.c src/edaPIC33LCDEXT.c src/edaPIC33OneWire.c src/edaPIC33OtherStuff.c src/edaPIC33OutputCompare.c src/edaPIC33PWM.c src/edaPIC33Protocol.c src/edaPIC33RingBuffer.c src/edaPIC33Setup.c src/edaPIC33SystemTime.c src/edaPIC33Timer.c src/edaPIC33UART1.c src/edaPIC33WS28B12.c src/edaPIC33Anemometer.c src/edaPIC33Time.c inc/edaPIC33Belueftung.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/src/Main_Test.o ${OBJECTDIR}/src/edaPIC33ADC.o ${OBJECTDIR}/src/edaPIC33BlinkLed.o ${OBJECTDIR}/src/edaPIC33DS12B20.o ${OBJECTDIR}/src/edaPIC33Hardware.o ${OBJECTDIR}/src/edaPIC33LCD.o ${OBJECTDIR}/src/edaPIC33LCD5110.o ${OBJECTDIR}/src/edaPIC33LCDEXT.o ${OBJECTDIR}/src/edaPIC33OneWire.o ${OBJECTDIR}/src/edaPIC33OtherStuff.o ${OBJECTDIR}/src/edaPIC33OutputCompare.o ${OBJECTDIR}/src/edaPIC33PWM.o ${OBJECTDIR}/src/edaPIC33Protocol.o ${OBJECTDIR}/src/edaPIC33RingBuffer.o ${OBJECTDIR}/src/edaPIC33Setup.o ${OBJECTDIR}/src/edaPIC33SystemTime.o ${OBJECTDIR}/src/edaPIC33Timer.o ${OBJECTDIR}/src/edaPIC33UART1.o ${OBJECTDIR}/src/edaPIC33WS28B12.o ${OBJECTDIR}/src/edaPIC33Anemometer.o ${OBJECTDIR}/src/edaPIC33Time.o ${OBJECTDIR}/inc/edaPIC33Belueftung.o
POSSIBLE_DEPFILES=${OBJECTDIR}/src/Main_Test.o.d ${OBJECTDIR}/src/edaPIC33ADC.o.d ${OBJECTDIR}/src/edaPIC33BlinkLed.o.d ${OBJECTDIR}/src/edaPIC33DS12B20.o.d ${OBJECTDIR}/src/edaPIC33Hardware.o.d ${OBJECTDIR}/src/edaPIC33LCD.o.d ${OBJECTDIR}/src/edaPIC33LCD5110.o.d ${OBJECTDIR}/src/edaPIC33LCDEXT.o.d ${OBJECTDIR}/src/edaPIC33OneWire.o.d ${OBJECTDIR}/src/edaPIC33OtherStuff.o.d ${OBJECTDIR}/src/edaPIC33OutputCompare.o.d ${OBJECTDIR}/src/edaPIC33PWM.o.d ${OBJECTDIR}/src/edaPIC33Protocol.o.d ${OBJECTDIR}/src/edaPIC33RingBuffer.o.d ${OBJECTDIR}/src/edaPIC33Setup.o.d ${OBJECTDIR}/src/edaPIC33SystemTime.o.d ${OBJECTDIR}/src/edaPIC33Timer.o.d ${OBJECTDIR}/src/edaPIC33UART1.o.d ${OBJECTDIR}/src/edaPIC33WS28B12.o.d ${OBJECTDIR}/src/edaPIC33Anemometer.o.d ${OBJECTDIR}/src/edaPIC33Time.o.d ${OBJECTDIR}/inc/edaPIC33Belueftung.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/src/Main_Test.o ${OBJECTDIR}/src/edaPIC33ADC.o ${OBJECTDIR}/src/edaPIC33BlinkLed.o ${OBJECTDIR}/src/edaPIC33DS12B20.o ${OBJECTDIR}/src/edaPIC33Hardware.o ${OBJECTDIR}/src/edaPIC33LCD.o ${OBJECTDIR}/src/edaPIC33LCD5110.o ${OBJECTDIR}/src/edaPIC33LCDEXT.o ${OBJECTDIR}/src/edaPIC33OneWire.o ${OBJECTDIR}/src/edaPIC33OtherStuff.o ${OBJECTDIR}/src/edaPIC33OutputCompare.o ${OBJECTDIR}/src/edaPIC33PWM.o ${OBJECTDIR}/src/edaPIC33Protocol.o ${OBJECTDIR}/src/edaPIC33RingBuffer.o ${OBJECTDIR}/src/edaPIC33Setup.o ${OBJECTDIR}/src/edaPIC33SystemTime.o ${OBJECTDIR}/src/edaPIC33Timer.o ${OBJECTDIR}/src/edaPIC33UART1.o ${OBJECTDIR}/src/edaPIC33WS28B12.o ${OBJECTDIR}/src/edaPIC33Anemometer.o ${OBJECTDIR}/src/edaPIC33Time.o ${OBJECTDIR}/inc/edaPIC33Belueftung.o

# Source Files
SOURCEFILES=src/Main_Test.c src/edaPIC33ADC.c src/edaPIC33BlinkLed.c src/edaPIC33DS12B20.c src/edaPIC33Hardware.c src/edaPIC33LCD.c src/edaPIC33LCD5110.c src/edaPIC33LCDEXT.c src/edaPIC33OneWire.c src/edaPIC33OtherStuff.c src/edaPIC33OutputCompare.c src/edaPIC33PWM.c src/edaPIC33Protocol.c src/edaPIC33RingBuffer.c src/edaPIC33Setup.c src/edaPIC33SystemTime.c src/edaPIC33Timer.c src/edaPIC33UART1.c src/edaPIC33WS28B12.c src/edaPIC33Anemometer.c src/edaPIC33Time.c inc/edaPIC33Belueftung.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/mplab.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33EP512MU810
MP_LINKER_FILE_OPTION=,--script=p33EP512MU810.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/src/Main_Test.o: src/Main_Test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/Main_Test.o.d 
	@${RM} ${OBJECTDIR}/src/Main_Test.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/Main_Test.c  -o ${OBJECTDIR}/src/Main_Test.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/Main_Test.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/Main_Test.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33ADC.o: src/edaPIC33ADC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33ADC.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33ADC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33ADC.c  -o ${OBJECTDIR}/src/edaPIC33ADC.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33ADC.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33ADC.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33BlinkLed.o: src/edaPIC33BlinkLed.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33BlinkLed.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33BlinkLed.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33BlinkLed.c  -o ${OBJECTDIR}/src/edaPIC33BlinkLed.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33BlinkLed.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33BlinkLed.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33DS12B20.o: src/edaPIC33DS12B20.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33DS12B20.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33DS12B20.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33DS12B20.c  -o ${OBJECTDIR}/src/edaPIC33DS12B20.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33DS12B20.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33DS12B20.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Hardware.o: src/edaPIC33Hardware.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Hardware.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Hardware.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Hardware.c  -o ${OBJECTDIR}/src/edaPIC33Hardware.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Hardware.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Hardware.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33LCD.o: src/edaPIC33LCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCD.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCD.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33LCD.c  -o ${OBJECTDIR}/src/edaPIC33LCD.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33LCD.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33LCD.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33LCD5110.o: src/edaPIC33LCD5110.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCD5110.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCD5110.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33LCD5110.c  -o ${OBJECTDIR}/src/edaPIC33LCD5110.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33LCD5110.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33LCD5110.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33LCDEXT.o: src/edaPIC33LCDEXT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCDEXT.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCDEXT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33LCDEXT.c  -o ${OBJECTDIR}/src/edaPIC33LCDEXT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33LCDEXT.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33LCDEXT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33OneWire.o: src/edaPIC33OneWire.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33OneWire.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33OneWire.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33OneWire.c  -o ${OBJECTDIR}/src/edaPIC33OneWire.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33OneWire.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33OneWire.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33OtherStuff.o: src/edaPIC33OtherStuff.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33OtherStuff.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33OtherStuff.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33OtherStuff.c  -o ${OBJECTDIR}/src/edaPIC33OtherStuff.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33OtherStuff.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33OtherStuff.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33OutputCompare.o: src/edaPIC33OutputCompare.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33OutputCompare.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33OutputCompare.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33OutputCompare.c  -o ${OBJECTDIR}/src/edaPIC33OutputCompare.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33OutputCompare.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33OutputCompare.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33PWM.o: src/edaPIC33PWM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33PWM.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33PWM.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33PWM.c  -o ${OBJECTDIR}/src/edaPIC33PWM.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33PWM.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33PWM.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Protocol.o: src/edaPIC33Protocol.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Protocol.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Protocol.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Protocol.c  -o ${OBJECTDIR}/src/edaPIC33Protocol.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Protocol.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Protocol.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33RingBuffer.o: src/edaPIC33RingBuffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33RingBuffer.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33RingBuffer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33RingBuffer.c  -o ${OBJECTDIR}/src/edaPIC33RingBuffer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33RingBuffer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33RingBuffer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Setup.o: src/edaPIC33Setup.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Setup.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Setup.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Setup.c  -o ${OBJECTDIR}/src/edaPIC33Setup.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Setup.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Setup.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33SystemTime.o: src/edaPIC33SystemTime.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33SystemTime.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33SystemTime.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33SystemTime.c  -o ${OBJECTDIR}/src/edaPIC33SystemTime.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33SystemTime.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33SystemTime.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Timer.o: src/edaPIC33Timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Timer.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Timer.c  -o ${OBJECTDIR}/src/edaPIC33Timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Timer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33UART1.o: src/edaPIC33UART1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33UART1.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33UART1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33UART1.c  -o ${OBJECTDIR}/src/edaPIC33UART1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33UART1.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33UART1.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33WS28B12.o: src/edaPIC33WS28B12.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33WS28B12.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33WS28B12.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33WS28B12.c  -o ${OBJECTDIR}/src/edaPIC33WS28B12.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33WS28B12.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33WS28B12.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Anemometer.o: src/edaPIC33Anemometer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Anemometer.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Anemometer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Anemometer.c  -o ${OBJECTDIR}/src/edaPIC33Anemometer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Anemometer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Anemometer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Time.o: src/edaPIC33Time.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Time.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Time.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Time.c  -o ${OBJECTDIR}/src/edaPIC33Time.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Time.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Time.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/inc/edaPIC33Belueftung.o: inc/edaPIC33Belueftung.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/inc" 
	@${RM} ${OBJECTDIR}/inc/edaPIC33Belueftung.o.d 
	@${RM} ${OBJECTDIR}/inc/edaPIC33Belueftung.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  inc/edaPIC33Belueftung.c  -o ${OBJECTDIR}/inc/edaPIC33Belueftung.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/inc/edaPIC33Belueftung.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/inc/edaPIC33Belueftung.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/src/Main_Test.o: src/Main_Test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/Main_Test.o.d 
	@${RM} ${OBJECTDIR}/src/Main_Test.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/Main_Test.c  -o ${OBJECTDIR}/src/Main_Test.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/Main_Test.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/Main_Test.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33ADC.o: src/edaPIC33ADC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33ADC.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33ADC.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33ADC.c  -o ${OBJECTDIR}/src/edaPIC33ADC.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33ADC.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33ADC.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33BlinkLed.o: src/edaPIC33BlinkLed.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33BlinkLed.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33BlinkLed.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33BlinkLed.c  -o ${OBJECTDIR}/src/edaPIC33BlinkLed.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33BlinkLed.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33BlinkLed.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33DS12B20.o: src/edaPIC33DS12B20.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33DS12B20.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33DS12B20.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33DS12B20.c  -o ${OBJECTDIR}/src/edaPIC33DS12B20.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33DS12B20.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33DS12B20.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Hardware.o: src/edaPIC33Hardware.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Hardware.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Hardware.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Hardware.c  -o ${OBJECTDIR}/src/edaPIC33Hardware.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Hardware.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Hardware.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33LCD.o: src/edaPIC33LCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCD.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCD.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33LCD.c  -o ${OBJECTDIR}/src/edaPIC33LCD.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33LCD.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33LCD.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33LCD5110.o: src/edaPIC33LCD5110.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCD5110.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCD5110.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33LCD5110.c  -o ${OBJECTDIR}/src/edaPIC33LCD5110.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33LCD5110.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33LCD5110.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33LCDEXT.o: src/edaPIC33LCDEXT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCDEXT.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33LCDEXT.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33LCDEXT.c  -o ${OBJECTDIR}/src/edaPIC33LCDEXT.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33LCDEXT.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33LCDEXT.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33OneWire.o: src/edaPIC33OneWire.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33OneWire.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33OneWire.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33OneWire.c  -o ${OBJECTDIR}/src/edaPIC33OneWire.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33OneWire.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33OneWire.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33OtherStuff.o: src/edaPIC33OtherStuff.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33OtherStuff.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33OtherStuff.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33OtherStuff.c  -o ${OBJECTDIR}/src/edaPIC33OtherStuff.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33OtherStuff.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33OtherStuff.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33OutputCompare.o: src/edaPIC33OutputCompare.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33OutputCompare.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33OutputCompare.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33OutputCompare.c  -o ${OBJECTDIR}/src/edaPIC33OutputCompare.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33OutputCompare.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33OutputCompare.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33PWM.o: src/edaPIC33PWM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33PWM.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33PWM.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33PWM.c  -o ${OBJECTDIR}/src/edaPIC33PWM.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33PWM.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33PWM.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Protocol.o: src/edaPIC33Protocol.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Protocol.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Protocol.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Protocol.c  -o ${OBJECTDIR}/src/edaPIC33Protocol.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Protocol.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Protocol.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33RingBuffer.o: src/edaPIC33RingBuffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33RingBuffer.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33RingBuffer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33RingBuffer.c  -o ${OBJECTDIR}/src/edaPIC33RingBuffer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33RingBuffer.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33RingBuffer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Setup.o: src/edaPIC33Setup.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Setup.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Setup.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Setup.c  -o ${OBJECTDIR}/src/edaPIC33Setup.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Setup.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Setup.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33SystemTime.o: src/edaPIC33SystemTime.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33SystemTime.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33SystemTime.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33SystemTime.c  -o ${OBJECTDIR}/src/edaPIC33SystemTime.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33SystemTime.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33SystemTime.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Timer.o: src/edaPIC33Timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Timer.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Timer.c  -o ${OBJECTDIR}/src/edaPIC33Timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Timer.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33UART1.o: src/edaPIC33UART1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33UART1.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33UART1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33UART1.c  -o ${OBJECTDIR}/src/edaPIC33UART1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33UART1.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33UART1.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33WS28B12.o: src/edaPIC33WS28B12.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33WS28B12.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33WS28B12.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33WS28B12.c  -o ${OBJECTDIR}/src/edaPIC33WS28B12.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33WS28B12.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33WS28B12.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Anemometer.o: src/edaPIC33Anemometer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Anemometer.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Anemometer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Anemometer.c  -o ${OBJECTDIR}/src/edaPIC33Anemometer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Anemometer.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Anemometer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/edaPIC33Time.o: src/edaPIC33Time.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/edaPIC33Time.o.d 
	@${RM} ${OBJECTDIR}/src/edaPIC33Time.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/edaPIC33Time.c  -o ${OBJECTDIR}/src/edaPIC33Time.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/edaPIC33Time.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/edaPIC33Time.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/inc/edaPIC33Belueftung.o: inc/edaPIC33Belueftung.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/inc" 
	@${RM} ${OBJECTDIR}/inc/edaPIC33Belueftung.o.d 
	@${RM} ${OBJECTDIR}/inc/edaPIC33Belueftung.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  inc/edaPIC33Belueftung.c  -o ${OBJECTDIR}/inc/edaPIC33Belueftung.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/inc/edaPIC33Belueftung.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/inc/edaPIC33Belueftung.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/mplab.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/mplab.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG=__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x1000:0x101B -mreserve=data@0x101C:0x101D -mreserve=data@0x101E:0x101F -mreserve=data@0x1020:0x1021 -mreserve=data@0x1022:0x1023 -mreserve=data@0x1024:0x1027 -mreserve=data@0x1028:0x104F   -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D__DEBUG=__DEBUG,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/mplab.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/mplab.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}/xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/mplab.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
