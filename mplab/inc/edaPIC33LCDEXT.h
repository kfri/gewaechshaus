/* 
 * File:   edaPIC33LCDEXT.h
 * Author: Kevin
 *
 * Created on 3. April 2018, 10:05
 */

#ifndef EDAPIC33LCDEXT_H
#define	EDAPIC33LCDEXT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h> // include processor files - each processor file is guarded.  
#include <stdint.h> 

// Control signal data pins
#define RW_EXT  LATDbits.LATD6  // LCD R/W signal
#define RS_EXT  LATFbits.LATF0 // LCD RS signal
#define E_EXT   LATDbits.LATD7  // LCD E signal

// Control signal pin direction
#define RW_TRIS_EXT TRISDbits.TRISD6
#define RS_TRIS_EXT TRISFbits.TRISF0
#define E_TRIS_EXT  TRISDbits.TRISD7

// Data signals and pin direction
#define DATA_EXT        LATD    // Port for LCD data
#define DATAPORT_EXT    PORTD
#define TRISDATA_EXT    TRISD   // I/O setup for data Port
    
#define cursor_rightExt()  sendCommandLCDExt( 0x14 )
#define cursor_leftExt()   sendCommandLCDExt( 0x10 )
#define display_shiftExt() sendCommandLCDExt( 0x1C )
#define home_clrExt()      sendCommandLCDExt( 0x01 )
#define home_itExt()       sendCommandLCDExt( 0x02 )
#define line_2Ext()        sendCommandLCDExt( 0xC0 ) //0b 1100 0000

//dijasio    
#define SetLCDExtG(a) sendCommandLCDExt((a&0x3F)|0x40)
#define SetLCDExtC(a) sendCommandLCDExt((a&0x7F)|0x80)
#define putLCDExt(d) writeDataLCDExt(d)


extern char DataStringExt[80];
extern char* pLCDExtDataStringLine1;
extern char* pLCDExtDataStringLine2;

/** 
 * @brief Initialisiere LCD Display
 * @param void
 * @return void
 * @details Funktion initialisiert das LCD Display.  Legt Pin Modes fest, initialisiert display mit cursor blink off & cursor off
 * @attention Blocking Code
 */
void initMyLCDExt();

/** 
 * @brief clockLCDenable
 * @param void
 * @return void
 * @details Funktion setzt setzt den Enable Eingang des LCD auf High und 4 Taktzyklen Später wieder auf Low
 */
void clockLCDExtenable();

/** 
 * @brief Sendet Character an den LCD
 * @param char c Character der an den LCD gesendet werden soll
 * @return void
 * @attention Blocking Code!
 */
void putcLCDExt(char c);

/** 
 * @brief Sendet String an den LCD
 * @param char* pData String der an den LCD gesendet werden soll
 * @return void
 * @attention Blocking Code!
 */
void putsLCDExt(char* pData);

/** 
 * @brief Sendet ein Command an den LCD
 * @param uint8_t ui8data Command das an den LCD gesendet werden
 * @return void
 * @attention Blocking Code!
 */
void sendCommandLCDExt(uint8_t ui8data);

/** 
 * @brief Sendet ein Command an den LCD
 * @param uint8_t ui8data Command das an den LCD gesendet werden
 * @return void
 * @attention NonBlocking Code!, readBusyFlagAndAddressLCD() has to be checked after calling this function!
 */

void sendCommandLCDExtNonBlocking(uint8_t ui8data);

/** 
 * @brief Sendet Daten an den LCD
 * @param uint8_t ui8data Daten die an den LCD gesendet werden sollen
 * @return void
 * @attention Blocking Code!
 */
void writeDataLCDExt(uint8_t ui8data);

/** 
 * @brief Sendet Daten an den LCD
 * @param uint8_t ui8data Daten die an den LCD gesendet werden sollen
 * @return void
 * @attention NonBlocking Code!, readBusyFlagAndAddressLCD() has to be checked after calling this function!
 */
void writeDataLCDExtNonBlocking(uint8_t ui8data);

/** 
 * @brief setzt DDRAM Adresse des LCD's
 * @param uint8_t ui8data Daten die an den LCD gesendet werden sollen
 * @return void
 * @attention Blocking Code!
 */
void setDDRAMAddressLCDExt(uint8_t ui8address);

/** 
 * @brief read busy flag
 * @param void
 * @return uint8_t BusyFlag
 * @details function reads the busy flag from lcd controller 1:controller is busy
 */
uint8_t readBusyFlagLCDExt();

/** 
 * @brief Sendet n Zeichen eines Strings an den LCD
 * @param char* pData String der an den LCD gesendet werden soll
 * @param uint8_t ui8n Anzahl der Zeichen die auf dem LCD angezeigt werden
 * @return void
 * @attention Blocking Code!
 */
void putncLCDExt(char* pData, uint8_t ui8n);

/** 
 * @brief clear LCD char storage
 * @param void
 * @return void
 * @details Funktion überschreibt den Schattenspeicher des LCDs
 */
void clearLCDExtStorage();

/** 
 * @brief Send Data to LCD
 * @param void
 * @return void
 * @details Funktion sendet bei jedem Aufruf einen Character des Schattenspeichers an den LCD
 * @attention Funktion muss zyklisch aufgerufen werden. Non-Blocking Funktion, liest das Busy Flag des LCD aus
 */
void sendDataToLCDExt();

/** 
 * @brief setLCDLine
 * @param const char* pStr String der in den Schattenspeicher gespeichert werden soll
 * @param uint8_t ui8Line Zeile in welcher der String gespeichert werden soll
 * @return void
 * @details Funktion kopiert einen String in den Schattenspeicher des LCDs
 * @attention Funktion überschreibt den Schattenspeicher des LCDs
 */
void setLCDExtLine(const char* pStr, uint8_t ui8Line);

/** 
 * @brief setLCDLine1
 * @param const char* pStr String der in den Schattenspeicher gespeichert werden soll
 * @return void
 * @details Funktion kopiert einen String in den Schattenspeicher (Line1) des LCDs
 * @attention Funktion überschreibt den Schattenspeicher des LCDs
 */
void setLCDExtLine1(const char* pString);

/** 
 * @brief setLCDLine2
 * @param const char* pStr String der in den Schattenspeicher gespeichert werden soll
 * @return void
 * @details Funktion kopiert einen String in den Schattenspeicher (Line2) des LCDs
 * @attention Funktion überschreibt den Schattenspeicher des LCDs
 */
void setLCDExtLine2(const char* pString);

//void putCharLCD(char c);

/** 
 * @brief create New Char for LCD Display
 * @param void
 * @return void 
 * @attention to add new chars, change function 
 */
//void createNewCharLcdExt();


#ifdef	__cplusplus
}
#endif

#endif	/* EDAPIC33LCDEXT_H */

