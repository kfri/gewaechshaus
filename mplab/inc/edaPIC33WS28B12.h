/* 
 * File:   edaPIC33WS28B12.h
 * Author: Kevin
 *
 * Created on 12. M�rz 2018, 17:18
 */

#ifndef EDAPIC33WS28B12_H
#define	EDAPIC33WS28B12_H

#include<stdint.h>
#include"edaPIC33Hardware.h"
#include<xc.h>
#ifdef	__cplusplus
extern "C" {
#endif

#define NUMOFLEDS 60

#define RED   0
#define GREEN 1
#define BLUE  2    
    
#define DOUT_WS2812 RD14
   
void initWS2812(void);
void sendDataWS2812(void);
void sendDataWS2812_2();

void clearLedValues(void);
void setAllLedValues(uint8_t uiR, uint8_t uiG, uint8_t uiB);
void sendWS2812Bit(uint8_t Bit);
void sendWS2812Bit_2(uint8_t Bit);
void doNops(uint16_t nops);

void setLed(uint8_t Num,uint8_t R, uint8_t G, uint8_t B);

void initLauflicht(void);
void onCycleLauflicht(uint8_t R, uint8_t G, uint8_t B);
void calcSmoothValues(uint8_t ui8Step, uint8_t ui8Max, uint8_t* puiR, uint8_t* puiG, uint8_t* puiB);
void calcRainbowValue(int16_t iAngle, uint8_t* pR, uint8_t* pG, uint8_t* pB);
void calcAllRainbowValues(uint16_t Offset);
#ifdef	__cplusplus
}
#endif

#endif	/* EDAPIC33WS28B12_H */

