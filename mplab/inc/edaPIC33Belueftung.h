/* 
 * File:   edaPIC33Belueftung.h
 * Author: root
 *
 * Created on 15. Juli 2018, 18:53
 */

#ifndef EDAPIC33BELUEFTUNG_H
#define	EDAPIC33BELUEFTUNG_H

#include "edaPIC33Hardware.h"


#ifdef	__cplusplus
extern "C" {
#endif

#define T_AUF 32000
#define T_ZU  28000
#define WIND_ZU 60
#define WIND_AUF 20
    
#define A1 9
#define A2 10    
    
#define MOTOR_CLOSE_TIME 30000    

#define STATE_AUTO_AUF  0
#define STATE_AUTO_ZU   1
#define STATE_AUTO_IDLE 2

#define STATE_MAN_AUF   3
#define STATE_MAN_ZU    4
#define STATE_MAN_IDLE  5
    
#define MODE_PIN 19
#define OPEN_PIN 22
#define CLOSE_PIN 21
   
void initBelueftung();
void onCycleBelueftung(int32_t Tin, int32_t Tout, uint16_t Wind, char* str);

#ifdef	__cplusplus
}
#endif

#endif	/* EDAPIC33BELUEFTUNG_H */

