/* 
 * File:   edaPIC33Oscillator.h
 * Author: Kevin
 *
 * Created on 26. April 2017, 18:15
 */

#ifndef EDAPIC33OSCILLATOR_H
#define	EDAPIC33OSCILLATOR_H

#include<xc.h>
#include<stdint.h>

//#include "./../inc/edaPIC33Hardware.h"



/** 
 * @brief config Oscillator with extern 8Mhz Crystal to Fosc=140Mhz
 * @attention Function has to be called at first in main 
 */
void configOscillator()
{
    PLLFBD = 68;
    CLKDIVbits.PLLPOST = 0;
    CLKDIVbits.PLLPRE  = 0;
    
    //Oszillatorwechsel einleiten
    OSCCONH = 0x03;
    OSCCONL = OSCCON | 0x01;
    
    
    while(OSCCONbits.COSC != 0b011); //warte bis Oszillatorwechsel stattgefunden hat 
    while(OSCCONbits.LOCK != 1);     //wate bis PLL eingeschwungen ist
}

#endif