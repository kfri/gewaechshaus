/* 
 * File:   edaPIC33Anemometer.h
 * Author: root
 *
 * Created on 15. Juli 2018, 17:10
 */

#ifndef EDAPIC33ANEMOMETER_H
#define	EDAPIC33ANEMOMETER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include<xc.h>
#include<stdint.h>
#include"edaPIC33OneWire.h"
#include"edaPIC33Hardware.h"

//#define ANEMOMETER_PIN 11
#define RESET_COUNTER_CALLS 10000
#define ANEMOMETER_PIN 23
    
void initAnemometer();
uint16_t onCycleAnemometer(void);
uint16_t getAnemometerCnt(void);
void resetAnemometerCnt(void);
        
#ifdef	__cplusplus
}
#endif

#endif	/* EDAPIC33ANEMOMETER_H */

