/* 
 * File:   edaPIC33LCD5110.h
 * Author: Kevin
 *
 * Created on 9. April 2018, 19:45
 */

#ifndef EDAPIC33LCD5110_H
#define	EDAPIC33LCD5110_H

#include "edaPIC33Hardware.h"


#ifdef	__cplusplus
extern "C" {
#endif

// the DC pin tells the LCD if we are sending a command or data
#define LCD_COMMAND 0
#define LCD_DATA  1

#define RESET   6
#define SCE     5
#define DC      4
#define SDIN    3
#define SCLK    1

/*Functions*/
    
void LCD5110_init();
void LCD5110_setContrast(uint16_t _contrast);
void LCD5110_setDimensions(int _COLS, int _ROWS);
void LCD5110_invert();
void LCD5110_gotoXY(int _x, int _y);
void LCD5110_bitmap(char _bitmapArray[]);
void LCD5110_progBitmap(char const _bitmapArray[]);
void LCD5110_character(char _character);
void LCD5110_string(char* _characters);
void LCD5110_clear(void);
void LCD5110_write(uint8_t _data_or_command, uint8_t _data);
void LCD5110_shiftOut(uint8_t  _data); //MSBFIRST

void clearLcd5110Storage(void);
void sendDataToLcd5110(void);
void setLcd5110StorageLine(char* str, uint8_t line);

#ifdef	__cplusplus
}
#endif

#endif	/* EDAPIC33LCD5110_H */

