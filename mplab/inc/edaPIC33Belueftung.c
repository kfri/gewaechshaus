#include <stdio.h>

#include"./../inc/edaPIC33Belueftung.h"
void openClapsAuto();
void closeClapsAuto();
void MotorAuf();
void MotorZu();
void MotorAus();

void initBelueftung()
{
    pinMode(MODE_PIN    ,DIGITAL_INPUT_PULLDOWN);
    pinMode(OPEN_PIN    ,DIGITAL_INPUT_PULLDOWN);
    pinMode(CLOSE_PIN   ,DIGITAL_INPUT_PULLDOWN);
    
    pinMode(A1          ,DIGITAL_OUTPUT);
    pinMode(A2          ,DIGITAL_OUTPUT);
    MotorAus();
}
    
void onCycleBelueftung(int32_t Tin, int32_t Tout, uint16_t Wind, char* str)
{
    static uint8_t ui8OldModePin = 0;
    static uint8_t ui8Mode = STATE_AUTO_IDLE;
    
    uint8_t ui8ModePin  = digitalRead(MODE_PIN); 
    uint8_t ui8OpenPin  = digitalRead(OPEN_PIN);
    uint8_t ui8ClosePin = digitalRead(CLOSE_PIN);
    
    //detect rising edge --> switch modes
    if(ui8OldModePin == 0 && ui8ModePin == 1)
    {
        //switch mode
        ui8Mode = ui8Mode < 3 ? STATE_MAN_IDLE : STATE_AUTO_IDLE;
    }
    ui8OldModePin = ui8ModePin;
    
    switch(ui8Mode)
    {
        case STATE_AUTO_AUF:
        {
            sprintf(str,"STATE_AUTO_AUF");
            if( Tin <= T_ZU /*|| Tout <= T_ZU*/ || Wind >= WIND_ZU )
            {
                ui8Mode = STATE_AUTO_ZU;
                MotorAus();
            }
            else
            {
                openClapsAuto();
            }
            break;
        }
        
        case STATE_AUTO_ZU:
        {
            sprintf(str,"STATE_AUTO_ZU");
            if(Wind <= WIND_AUF && Tin >= T_AUF /*&& Tout > T_ZU*/)
            {
                ui8Mode = STATE_AUTO_AUF;
                MotorAus();
            }
            else
            {
                closeClapsAuto();
            }
            break;
        }
        
        case STATE_AUTO_IDLE:
        {
            sprintf(str,"STATE_AUTO_IDLE");
            ui8Mode = STATE_AUTO_ZU;
            MotorAus();
            break;
        }
        case STATE_MAN_AUF:
        {
            sprintf(str,"STATE_MAN_AUF");
            if(ui8OpenPin == 1 && ui8ClosePin == 0)
            {
                MotorAuf();
            }
            else
            {
                ui8Mode = STATE_MAN_IDLE;
                MotorAus();
            }
            break;
        }
        
        case STATE_MAN_ZU:
        {
            sprintf(str,"STATE_MAN_ZU");
            if(ui8OpenPin == 0 && ui8ClosePin == 1)
            {
                MotorZu();
            }
            else
            {
                ui8Mode = STATE_MAN_IDLE;
                MotorAus();
            }
            break;
        }
        
        case STATE_MAN_IDLE:
        {
            sprintf(str,"STATE_MAN_IDLE");
            MotorAus();
            if(ui8OpenPin == 1 && ui8ClosePin == 0)
            {
                ui8Mode = STATE_MAN_AUF;
                MotorAus();
            }
            if(ui8OpenPin == 0 && ui8ClosePin == 1)
            {
                ui8Mode = STATE_MAN_ZU;
                MotorAus();
            }
                
            break;
        }
    }
    
}

void openClapsAuto()
{
    static uint16_t ui32Calls = 0;
    ui32Calls++;
    if(ui32Calls < MOTOR_CLOSE_TIME)
    {
        MotorAuf();
    }
    else
    {
        ui32Calls=0;
        MotorAus();
    }
}

void closeClapsAuto()
{
    static uint16_t ui32Calls = 0;
    ui32Calls++;
    if(ui32Calls < MOTOR_CLOSE_TIME)
    {
        MotorZu();
    }
    else
    {
        ui32Calls=0;
        MotorAus();
    }
}

void MotorAuf()
{
    digitalWrite(A1,1);
    digitalWrite(A2,0);
}

void MotorZu()
{
    digitalWrite(A1,0);
    digitalWrite(A2,1);
}

void MotorAus()
{
    digitalWrite(A1,0);
    digitalWrite(A2,0);
}
 
