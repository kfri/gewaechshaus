/* 
 * File:   edaPIC33DS12B20.h
 * Author: Kevin
 *
 * Created on 2. April 2018, 20:57
 */

#ifndef EDAPIC33DS12B20_H
#define	EDAPIC33DS12B20_H

#ifdef	__cplusplus
extern "C" {
#endif

#include<xc.h>
#include<stdint.h>
#include"edaPIC33OneWire.h"
#include"edaPIC33Hardware.h"
      
//#define DS12B20_Pin 7

int32_t getTempDS12B20OnCycle_1(uint8_t ui8Pin);
void    initDS12B20_1(uint8_t ui8Pin);
int32_t getTempDS12B20OnCycle_2(uint8_t ui8Pin);
void    initDS12B20_2(uint8_t ui8Pin);

#ifdef	__cplusplus
}
#endif

#endif	/* EDAPIC33DS12B20_H */

