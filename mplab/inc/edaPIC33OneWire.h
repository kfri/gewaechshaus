/* 
 * File:   edaPIC33DS12B20.h
 * Author: Kevin
 *
 * Created on 2. April 2018, 15:02
 */

#ifndef EDAPIC33ONEWIRE_H
#define	EDAPIC33ONEWIRE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <libpic30.h>

#include "edaPIC33Hardware.h"    

/*DELAYs in us*/
//A	Standard	6       Overdrive	1.0
#define DELAY_A 420
//B	Standard	64      Overdrive	7.5
#define DELAY_B 4480
//C	Standard	60      Overdrive	7.5
#define DELAY_C 4200
//D	Standard	10      Overdrive	2.5
#define DELAY_D 700
//E	Standard	9       Overdrive	1.0
#define DELAY_E 630
//F	Standard	55      Overdrive	7
#define DELAY_F 3850
//G	Standard	0       Overdrive	2.5
#define DELAY_G 0
//H	Standard	480     Overdrive	70
#define DELAY_H 33600
//I	Standard	70      Overdrive	8.5
#define DELAY_I 4900
//J	Standard	410     Overdrive	40
#define DELAY_J 28700    
   
//-----------------------------------------------------------------------------
// Generate a 1-Wire reset, return 1 if no presence detect was found,
// return 0 otherwise.
// (NOTE: Does not handle alarm presence from DS2404/DS1994)
//
int OWTouchReset(uint8_t ui8Pin);

//-----------------------------------------------------------------------------
// Send a 1-Wire write bit. Provide 10us recovery time.
//
void OWWriteBit(uint8_t ui8Pin,int bit);
//-----------------------------------------------------------------------------
// Read a bit from the 1-Wire bus and return it. Provide 10us recovery time.
//
int OWReadBit(uint8_t ui8Pin);

//-----------------------------------------------------------------------------
// Write 1-Wire data byte
//
void OWWriteByte(uint8_t ui8Pin,int data);

//-----------------------------------------------------------------------------
// Read 1-Wire data byte and return it
//
int OWReadByte(uint8_t ui8Pin);

//-----------------------------------------------------------------------------
// Write a 1-Wire data byte and return the sampled result.
//
int OWTouchByte(uint8_t ui8Pin,int data);

//-----------------------------------------------------------------------------
// Write a block 1-Wire data bytes and return the sampled result in the same
// buffer.
//
void OWBlock(uint8_t ui8Pin,unsigned char *data, int data_len);

/*
//-----------------------------------------------------------------------------
// Set all devices on 1-Wire to overdrive speed. Return '1' if at least one
// overdrive capable device is detected.
//
int OWOverdriveSkip(unsigned char *data, int data_len);
*/

#ifdef	__cplusplus
}
#endif

#endif	/* EDAPIC33ONEWIRE_H */

