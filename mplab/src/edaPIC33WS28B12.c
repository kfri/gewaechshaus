#include"./../inc/edaPIC33WS28B12.h"
#include "./../inc/edaPIC33Setup.h"


uint8_t auiLedValuesWS2812[NUMOFLEDS][3];


void initWS2812(void)
{
    //16bit free running timer, without prescaler
    T3CON = 0x2000; //0b0010 0000 0000 0000;
    IPC2bits.T3IP = 0;  //Interrupt Priority Level
    IFS0bits.T3IF = 0;  //clear Interrupt Flag
    IEC0bits.T3IE = 0;  //disable Timer 3 Interrupt
    TMR3 = 0;
    PR3 = 0xFFFF;
    T3CONbits.TON=1;

    pinMode(DOUT_WS2812,DIGITAL_OUTPUT);
}

void clearLedValues(void)
{
    uint16_t i,j;
    
    for(i=0; i<NUMOFLEDS;i++)
    {
        for(j=0; j<3;j++)
        {
            auiLedValuesWS2812[i][j] = 0;
        }
    }
}

void setAllLedValues(uint8_t R, uint8_t G, uint8_t B)
{
    uint16_t i,j;
    
    for(i=0; i<NUMOFLEDS;i++)
    {
        for(j=0; j<3;j++)
        {
            auiLedValuesWS2812[i][j] = j==RED?G:(j==GREEN?R:B);
        }
    }
}

void setLed(uint8_t Num,uint8_t R, uint8_t G, uint8_t B)
{
    auiLedValuesWS2812[Num][RED]   = R;
    auiLedValuesWS2812[Num][GREEN] = G;
    auiLedValuesWS2812[Num][BLUE]  = B;
}

void sendDataWS2812_2()
{
    uint16_t i,Num;
    uint8_t temp1,temp2,temp3;
    
    for(Num=0; Num <NUMOFLEDS;Num++)
    {
            temp1 = auiLedValuesWS2812[Num][0];
            temp2 = auiLedValuesWS2812[Num][1];
            temp3 = auiLedValuesWS2812[Num][2];
            for(i=0; i<8;i++)
            {
                sendWS2812Bit_2(temp1&0x80);
                temp1<<=1;
            }
            for(i=0; i<8;i++)
            {
                sendWS2812Bit_2(temp2&0x80);
                temp2<<=1;
            }  
            for(i=0; i<8;i++)
            {
                sendWS2812Bit_2(temp3&0x80);
                temp3<<=1;
            }  
    }
}
/*
void sendWS2812Bit(uint8_t Bit)
{
   if (Bit)
   { 
    PORTD&=0xBFFF;
    doNops(16); //16
    PORTD|=0x4000;
   }
   else
   { 
    PORTD&=0xBFFF;
    doNops(7);//7
    PORTD|=0x4000;
    doNops(1);
   }
}
*/
void sendWS2812Bit_2(uint8_t Bit)
{
   TMR3=0;
   if (Bit)
   {  
    PORTD&=0xBFFF;
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    PORTD|=0x4000;
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
   }
   else
   { 
    PORTD&=0xBFFF;
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    PORTD|=0x4000;   
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
   }
}
/*
void sendWS2812Bit_2(uint8_t Bit)
{
   TMR3=0;
   if (Bit)
   {  
    PORTD&=0xBFFF;
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    PORTD|=0x4000;
   }
   else
   { 
    PORTD&=0xBFFF;
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    PORTD|=0x4000;   
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
    Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
   }
} 
 */
void __attribute__((__interrupt__, no_auto_psv)) _T3Interrupt(void)
{
    digitalToggle(DOUT_WS2812);
    IFS0bits.T3IF = 0;      //Clear Timer3 interrupt flag
}

void doNops(uint16_t nops)
{
    uint16_t i;
    for(i=0;i<nops;i++)
        Nop();
}

uint8_t ui8LauflichtNum;
void initLauflicht(void)
{
    ui8LauflichtNum=0;
    clearLedValues();
}

void onCycleLauflicht(uint8_t R, uint8_t G, uint8_t B)
{
    setLed(ui8LauflichtNum,0,0,0);
    ui8LauflichtNum++;
    if(ui8LauflichtNum>=NUMOFLEDS)
        ui8LauflichtNum=0;
    setLed(ui8LauflichtNum,R,G,B);
}

void calcSmoothValues(uint8_t ui8Step, uint8_t ui8Max, uint8_t* puiR, uint8_t* puiG, uint8_t* puiB)
{
    static uint8_t R=0;
    static uint8_t G=0;
    static uint8_t B=0;
    static uint8_t State=0x00;
    
    switch(State)
    {
        case 0x00:
            R += ui8Step;
            if(R>=ui8Max)
                State = 0x02;
            break;
            
        case 0x02:
            G += ui8Step;
            if(G>=ui8Max)
                State = 0x03;
            break;
            
        case 0x03:
            B += ui8Step;
            if(B>=ui8Max)
                State = 0x07;
            break;
            
        case 0x07:
            if(ui8Step>B)
            {
                State = 0x06;
                B=0;
            }
            else
                B -= ui8Step;
            break;
            
        case 0x06:
            if(ui8Step>G)
            {
                State = 0x04;
                G=0;
            }
            else
                G -= ui8Step;
            break;
            
        case 0x04:
            B += ui8Step;
            if(B>=ui8Max)
                State = 0x05;
            break;
            
        case 0x05:
            if(ui8Step>R)
            {
                State = 0x01;
                R=0;
            }
            else
                R -= ui8Step;
            break;
            
        case 0x01:
            if(ui8Step>B)
            {
                State = 0x00;
                B=0;
            }
            else
                B -= ui8Step;
            break;
    }
    
    *puiR=R;
    *puiG=G;
    *puiB=B;
}

void calcAllRainbowValues(uint16_t Offset)
{
    int i;
    uint8_t ui8DegStep=0;
    ui8DegStep = 360/NUMOFLEDS;
    
    
    for(i=0;i<NUMOFLEDS;i++)
    {
        uint8_t R,G,B;
        int16_t val = i * ui8DegStep + Offset;
        while(val > 360)
            val-=360;
        
        calcRainbowValue(val,&R,&G,&B);
        setLed(i,R,G,B);                
    }
        

}

/***********************************************************************************************************************************************************/
/*Rainbow calculation*/
const uint8_t lights[360]={
  0,   0,   0,   0,   0,   1,   1,   2, 
  2,   3,   4,   5,   6,   7,   8,   9, 
 11,  12,  13,  15,  17,  18,  20,  22, 
 24,  26,  28,  30,  32,  35,  37,  39, 
 42,  44,  47,  49,  52,  55,  58,  60, 
 63,  66,  69,  72,  75,  78,  81,  85, 
 88,  91,  94,  97, 101, 104, 107, 111, 
114, 117, 121, 124, 127, 131, 134, 137, 
141, 144, 147, 150, 154, 157, 160, 163, 
167, 170, 173, 176, 179, 182, 185, 188, 
191, 194, 197, 200, 202, 205, 208, 210, 
213, 215, 217, 220, 222, 224, 226, 229, 
231, 232, 234, 236, 238, 239, 241, 242, 
244, 245, 246, 248, 249, 250, 251, 251, 
252, 253, 253, 254, 254, 255, 255, 255, 
255, 255, 255, 255, 254, 254, 253, 253, 
252, 251, 251, 250, 249, 248, 246, 245, 
244, 242, 241, 239, 238, 236, 234, 232, 
231, 229, 226, 224, 222, 220, 217, 215, 
213, 210, 208, 205, 202, 200, 197, 194, 
191, 188, 185, 182, 179, 176, 173, 170, 
167, 163, 160, 157, 154, 150, 147, 144, 
141, 137, 134, 131, 127, 124, 121, 117, 
114, 111, 107, 104, 101,  97,  94,  91, 
 88,  85,  81,  78,  75,  72,  69,  66, 
 63,  60,  58,  55,  52,  49,  47,  44, 
 42,  39,  37,  35,  32,  30,  28,  26, 
 24,  22,  20,  18,  17,  15,  13,  12, 
 11,   9,   8,   7,   6,   5,   4,   3, 
  2,   2,   1,   1,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0, 
  0,   0,   0,   0,   0,   0,   0,   0};

const uint8_t HSVlights[61] = 
{0, 4, 8, 13, 17, 21, 25, 30, 34, 38, 42, 47, 51, 55, 59, 64, 68, 72, 76,
81, 85, 89, 93, 98, 102, 106, 110, 115, 119, 123, 127, 132, 136, 140, 144,
149, 153, 157, 161, 166, 170, 174, 178, 183, 187, 191, 195, 200, 204, 208,
212, 217, 221, 225, 229, 234, 238, 242, 246, 251, 255};

const uint8_t HSVpower[121] = 
{0, 2, 4, 6, 8, 11, 13, 15, 17, 19, 21, 23, 25, 28, 30, 32, 34, 36, 38, 40,
42, 45, 47, 49, 51, 53, 55, 57, 59, 62, 64, 66, 68, 70, 72, 74, 76, 79, 81, 
83, 85, 87, 89, 91, 93, 96, 98, 100, 102, 104, 106, 108, 110, 113, 115, 117, 
119, 121, 123, 125, 127, 130, 132, 134, 136, 138, 140, 142, 144, 147, 149, 
151, 153, 155, 157, 159, 161, 164, 166, 168, 170, 172, 174, 176, 178, 181, 
183, 185, 187, 189, 191, 193, 195, 198, 200, 202, 204, 206, 208, 210, 212, 
215, 217, 219, 221, 223, 225, 227, 229, 232, 234, 236, 238, 240, 242, 244, 
246, 249, 251, 253, 255};

// the real HSV rainbow
void trueHSV(int angle,uint8_t*red,uint8_t*green,uint8_t*blue)
{
  if (angle<60) {*red = 255; *green = HSVlights[angle]; *blue = 0;} else
  if (angle<120) {*red = HSVlights[120-angle]; *green = 255; *blue = 0;} else 
  if (angle<180) {*red = 0, *green = 255; *blue = HSVlights[angle-120];} else 
  if (angle<240) {*red = 0, *green = HSVlights[240-angle]; *blue = 255;} else 
  if (angle<300) {*red = HSVlights[angle-240], *green = 0; *blue = 255;} else 
                 {*red = 255, *green = 0; *blue = HSVlights[360-angle];} 
  //setRGBpoint(LED, red, green, blue);
}
/*
// the 'power-conscious' HSV rainbow
void powerHSV(byte LED, int angle)
{
  byte red, green, blue;
  if (angle<120) {red = HSVpower[120-angle]; green = HSVpower[angle]; blue = 0;} else
  if (angle<240) {red = 0;  green = HSVpower[240-angle]; blue = HSVpower[angle-120];} else
                 {red = HSVpower[angle-240]; green = 0; blue = HSVpower[360-angle];}
    setRGBpoint(LED, red, green, blue);
}
*/

/*
// sine wave rainbow
void sineLED(byte LED, int angle)
{
  setRGBpoint(LED, lights[(angle+120)%360], lights[angle],  lights[(angle+240)%360]);
}
*/

/*
 iAngle 0...359
 */
void calcRainbowValue(int16_t iAngle, uint8_t* pR, uint8_t* pG, uint8_t* pB)
{
    trueHSV(iAngle,pR,pG,pB);
}