#include"./../inc/edaPIC33Anemometer.h"
#include"./../inc/edaPIC33OtherStuff.h"
#include"./../inc/edaPIC33ADC.h"

uint16_t ui16AnemometerCnt = 0;
uint16_t ui16OldAnemometerCnt = 0;

void initAnemometer()
{
    pinMode(ANEMOMETER_PIN,DIGITAL_INPUT_PULLUP);
}

// check in and outputs
// return counter value
// reset Counter after RESET_COUNTER_CALLS calls
uint16_t onCycleAnemometer(void)
{
    
    static uint8_t ui8OldPinVal = 0;
    uint8_t ui8ActPinVal = analogRead(ANEMOMETER_PIN) > 512 ? 1 : 0;
    ui8ActPinVal = isDebounceAnemometer(ui8ActPinVal);
    static uint16_t ui16Calls = 0;
    
    ui16Calls++;
    
    if(ui16Calls%RESET_COUNTER_CALLS == 0)
    {
        ui16OldAnemometerCnt = ui16AnemometerCnt;
        ui16AnemometerCnt = 0;
        ui16Calls=0;
    }
    
    //detect rising edge and count
    if(ui8OldPinVal==0 && ui8ActPinVal==1)
    {
        ui16AnemometerCnt++;
    }
            
    ui8OldPinVal = ui8ActPinVal;
    return ui16OldAnemometerCnt;
}

//return Counter Value
uint16_t getAnemometerCnt(void)
{
    return ui16OldAnemometerCnt;
}

void resetAnemometerCnt(void)
{
    ui16AnemometerCnt = 0;
}

