/** 
 * @file   PWM_Main.c
 * @author Kevin Fritz
 *
 * @date 26.04.2017, 18:14
 */



// Check for Project Settings
#ifndef __dsPIC33EP512MU810__
    #error "Wrong Controller"
#endif


/* ***********************
 * Includes
 * ***********************
 */

#include <xc.h>                 //Include appropriate controller specific headers
#include <stdint.h>             //Standard typedefs
#include <stdio.h>              //Standard IO Library
#include "./../inc/edaPIC33SystemTime.h" //edaPIC33 System Time Library, verwendet Timer1!

#include "./../inc/edaPIC33LCD.h"        //edaPIC33 LCD Library
#include "./../inc/edaPIC33OtherStuff.h" //other Functions
#include "./../inc/edaPIC33Setup.h"      //setup library for EDAPIC33 Board
#include "./../inc/edaPIC33OutputCompare.h"
#include "./../inc/edaPIC33ADC.h"
#include "./../inc/edaPIC33UART1.h"
#include "./../inc/edaPIC33Protocol.h"
#include "./../inc/edaPIC33WS28B12.h"
#include "./../inc/edaPIC33DS12B20.h"
#include "./../inc/edaPIC33LCDEXT.h"
#include "./../inc/edaPIC33LCD5110.h"
#include "./../inc/edaPIC33Anemometer.h"
#include "./../inc/edaPIC33Belueftung.h"
#include <libpic30.h>

#include "./../inc/edaPIC33Oscillator.h" //Oscillator Library, konfiguriert Oscillator
#include "./../inc/edaPIC33Hardware.h"   //edaPIC33 Hardware Library
/* ***********************
 * Configuration Bits
 * ***********************
 */
//select external oscillator with 8mhz
_FOSCSEL(FNOSC_PRIPLL & IESO_OFF); //Initial Oscillator: Primary Oscillator (XT, HS, EC) with PLL
_FOSC(FCKSM_CSECMD & OSCIOFNC_OFF & POSCMD_XT);  //HS Crystal Oscillator Mode
#pragma config ICS = PGD3


/* ***********************
 * Defines
 * ***********************
 */

/* Substitute for stdlib.h */
#define	EXIT_SUCCESS	0
#define	EXIT_FAILURE	1

//#define CTS _RF12
//#define RTS _RF13
#define TRTS TRISFbits.TRISF13

/* ***********************
 * Prototypes
 * ***********************
 */
/* ***********************
 * Definitions
 * ***********************
 */
/*String used in edaPIC33LCD Library*/
extern char DataString[32];
char str[16];
char szRX[32] = "\0";
char szTX[32] = "\0";

extern char psUart1ReadBuffer[256];

/* ***********************
 * Main
 * ***********************
 */
/**
 * Main Function
 */
int main() {
    configOscillator();
    
    //config timer 1 for getSystemTimeMillis();)
    configSystemTimeMillis();
    uint32_t ui32Time= getSystemTimeMillis(); //Variable used for time calculation
    //initWS2812();
    //setAllLedValues(0, 0, 0);
    
    //initDS12B20();
    
    LCD5110_init();
    LCD5110_clear();
    clearLcd5110Storage();
    
    initDS12B20_1((uint8_t) 7);
    initDS12B20_2((uint8_t) 8);
 
    //initAnemometer(); //old: 23
    pinMode(ANEMOMETER_PIN,ANALOG_INPUT);
    setPulldown(ANEMOMETER_PIN);
    initBelueftung();
    
    initADC1();
    
    // Endless Loop
    while(1){         
        
        int32_t i32Tin = getTempDS12B20OnCycle_1((uint8_t) 7);
        int32_t i32Tout = getTempDS12B20OnCycle_2((uint8_t) 8);        
        uint16_t ui16Wind = onCycleAnemometer();
        
        onCycleBelueftung(i32Tin,i32Tout,ui16Wind,str);
        setLcd5110StorageLine(str, 1); //print state from bel�ftung to lcd
        
        if(ui32Time%10==0)
        {
            sprintf(str,"%lu",ui32Time/1000);
            setLcd5110StorageLine(str, 0);
        
            sprintf(str,"Tin: %2.2f C",((float)i32Tin) /1000.0);
            setLcd5110StorageLine(str, 3);

            sprintf(str,"Tout:%2.2f C",((float)i32Tout) /1000.0);
            setLcd5110StorageLine(str, 4);

            sprintf(str,"Wind: %u   %u", ui16Wind,analogRead(ANEMOMETER_PIN));
            setLcd5110StorageLine(str, 5);
        
            sendDataToLcd5110();
        }
        ClrWdt();
        ui32Time++; //increase ms counter
        while(getSystemTimeMillis() < ui32Time) //wait rest of 1ms
        {
            ClrWdt();   //clear watchdog timer
        }
    }//while
    
    return (EXIT_SUCCESS);  //never reached
} //main()
