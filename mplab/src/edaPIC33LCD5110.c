#include"./../inc/edaPIC33LCD5110.h"
#include "./../inc/edaPIC33Setup.h"

char LcdStorage[14][6] = {};

void clearLcd5110Storage(void)
{
    uint8_t i,j;
    for(i=0; i<6;i++)
        for(j=0; j<14;j++)
            LcdStorage[j][i] = ' ';
}

void sendDataToLcd5110(void)
{
    static uint8_t j=0;
    static uint8_t i=0;
    LCD5110_character(LcdStorage[j++][i]);
    if(j==14)
    {
        j=0;
        i++;
        if(i==6) i=0;
    }
}

void sendCompleteDataToLcd5110(void)
{
    uint8_t i,j;
    for(i=0;i<6;i++)
        for(j=0;j<14;j++)
            LCD5110_character(LcdStorage[j][i]);
}

void setLcd5110StorageLine(char* str, uint8_t line)
{
    uint8_t i=0;
    
    while(str[i] != '\0' && i<14)
    {
        LcdStorage[i][line]=str[i];
        i++;
    }
    
    while(i<14)
        LcdStorage[i++][line]=' ';
}

// This table contains the hex values that represent pixels
// for a font that is 5 pixels wide and 8 pixels high
const uint8_t ASCII[][5] = 
{
	{0x00, 0x00, 0x00, 0x00, 0x00} // 20
	, {0x00, 0x00, 0x5f, 0x00, 0x00} // 21 !
	, {0x00, 0x07, 0x00, 0x07, 0x00} // 22 "
	, {0x14, 0x7f, 0x14, 0x7f, 0x14} // 23 #
	, {0x24, 0x2a, 0x7f, 0x2a, 0x12} // 24 $
	, {0x23, 0x13, 0x08, 0x64, 0x62} // 25 %
	, {0x36, 0x49, 0x55, 0x22, 0x50} // 26 &
	, {0x00, 0x05, 0x03, 0x00, 0x00} // 27 '
	, {0x00, 0x1c, 0x22, 0x41, 0x00} // 28 (
	, {0x00, 0x41, 0x22, 0x1c, 0x00} // 29 )
	, {0x14, 0x08, 0x3e, 0x08, 0x14} // 2a *
	, {0x08, 0x08, 0x3e, 0x08, 0x08} // 2b +
	, {0x00, 0x50, 0x30, 0x00, 0x00} // 2c ,
	, {0x08, 0x08, 0x08, 0x08, 0x08} // 2d -
	, {0x00, 0x60, 0x60, 0x00, 0x00} // 2e .
	, {0x20, 0x10, 0x08, 0x04, 0x02} // 2f /
	, {0x3e, 0x51, 0x49, 0x45, 0x3e} // 30 0
	, {0x00, 0x42, 0x7f, 0x40, 0x00} // 31 1
	, {0x42, 0x61, 0x51, 0x49, 0x46} // 32 2
	, {0x21, 0x41, 0x45, 0x4b, 0x31} // 33 3
	, {0x18, 0x14, 0x12, 0x7f, 0x10} // 34 4
	, {0x27, 0x45, 0x45, 0x45, 0x39} // 35 5
	, {0x3c, 0x4a, 0x49, 0x49, 0x30} // 36 6
	, {0x01, 0x71, 0x09, 0x05, 0x03} // 37 7
	, {0x36, 0x49, 0x49, 0x49, 0x36} // 38 8
	, {0x06, 0x49, 0x49, 0x29, 0x1e} // 39 9
	, {0x00, 0x36, 0x36, 0x00, 0x00} // 3a :
	, {0x00, 0x56, 0x36, 0x00, 0x00} // 3b ;
	, {0x08, 0x14, 0x22, 0x41, 0x00} // 3c <
	, {0x14, 0x14, 0x14, 0x14, 0x14} // 3d =
	, {0x00, 0x41, 0x22, 0x14, 0x08} // 3e >
	, {0x02, 0x01, 0x51, 0x09, 0x06} // 3f ?
	, {0x32, 0x49, 0x79, 0x41, 0x3e} // 40 @
	, {0x7e, 0x11, 0x11, 0x11, 0x7e} // 41 A
	, {0x7f, 0x49, 0x49, 0x49, 0x36} // 42 B
	, {0x3e, 0x41, 0x41, 0x41, 0x22} // 43 C
	, {0x7f, 0x41, 0x41, 0x22, 0x1c} // 44 D
	, {0x7f, 0x49, 0x49, 0x49, 0x41} // 45 E
	, {0x7f, 0x09, 0x09, 0x09, 0x01} // 46 F
	, {0x3e, 0x41, 0x49, 0x49, 0x7a} // 47 G
	, {0x7f, 0x08, 0x08, 0x08, 0x7f} // 48 H
	, {0x00, 0x41, 0x7f, 0x41, 0x00} // 49 I
	, {0x20, 0x40, 0x41, 0x3f, 0x01} // 4a J
	, {0x7f, 0x08, 0x14, 0x22, 0x41} // 4b K
	, {0x7f, 0x40, 0x40, 0x40, 0x40} // 4c L
	, {0x7f, 0x02, 0x0c, 0x02, 0x7f} // 4d M
	, {0x7f, 0x04, 0x08, 0x10, 0x7f} // 4e N
	, {0x3e, 0x41, 0x41, 0x41, 0x3e} // 4f O
	, {0x7f, 0x09, 0x09, 0x09, 0x06} // 50 P
	, {0x3e, 0x41, 0x51, 0x21, 0x5e} // 51 Q
	, {0x7f, 0x09, 0x19, 0x29, 0x46} // 52 R
	, {0x46, 0x49, 0x49, 0x49, 0x31} // 53 S
	, {0x01, 0x01, 0x7f, 0x01, 0x01} // 54 T
	, {0x3f, 0x40, 0x40, 0x40, 0x3f} // 55 U
	, {0x1f, 0x20, 0x40, 0x20, 0x1f} // 56 V
	, {0x3f, 0x40, 0x38, 0x40, 0x3f} // 57 W
	, {0x63, 0x14, 0x08, 0x14, 0x63} // 58 X
	, {0x07, 0x08, 0x70, 0x08, 0x07} // 59 Y
	, {0x61, 0x51, 0x49, 0x45, 0x43} // 5a Z
	, {0x00, 0x7f, 0x41, 0x41, 0x00} // 5b [
	, {0x02, 0x04, 0x08, 0x10, 0x20} // 5c backslash
	, {0x00, 0x41, 0x41, 0x7f, 0x00} // 5d ]
	, {0x04, 0x02, 0x01, 0x02, 0x04} // 5e ^
	, {0x40, 0x40, 0x40, 0x40, 0x40} // 5f _
	, {0x00, 0x01, 0x02, 0x04, 0x00} // 60 `
	, {0x20, 0x54, 0x54, 0x54, 0x78} // 61 a
	, {0x7f, 0x48, 0x44, 0x44, 0x38} // 62 b
	, {0x38, 0x44, 0x44, 0x44, 0x20} // 63 c
	, {0x38, 0x44, 0x44, 0x48, 0x7f} // 64 d
	, {0x38, 0x54, 0x54, 0x54, 0x18} // 65 e
	, {0x08, 0x7e, 0x09, 0x01, 0x02} // 66 f
	, {0x0c, 0x52, 0x52, 0x52, 0x3e} // 67 g
	, {0x7f, 0x08, 0x04, 0x04, 0x78} // 68 h
	, {0x00, 0x44, 0x7d, 0x40, 0x00} // 69 i
	, {0x20, 0x40, 0x44, 0x3d, 0x00} // 6a j
	, {0x7f, 0x10, 0x28, 0x44, 0x00} // 6b k
	, {0x00, 0x41, 0x7f, 0x40, 0x00} // 6c l
	, {0x7c, 0x04, 0x18, 0x04, 0x78} // 6d m
	, {0x7c, 0x08, 0x04, 0x04, 0x78} // 6e n
	, {0x38, 0x44, 0x44, 0x44, 0x38} // 6f o
	, {0x7c, 0x14, 0x14, 0x14, 0x08} // 70 p
	, {0x08, 0x14, 0x14, 0x18, 0x7c} // 71 q
	, {0x7c, 0x08, 0x04, 0x04, 0x08} // 72 r
	, {0x48, 0x54, 0x54, 0x54, 0x20} // 73 s
	, {0x04, 0x3f, 0x44, 0x40, 0x20} // 74 t
	, {0x3c, 0x40, 0x40, 0x20, 0x7c} // 75 u
	, {0x1c, 0x20, 0x40, 0x20, 0x1c} // 76 v
	, {0x3c, 0x40, 0x30, 0x40, 0x3c} // 77 w
	, {0x44, 0x28, 0x10, 0x28, 0x44} // 78 x
	, {0x0c, 0x50, 0x50, 0x50, 0x3c} // 79 y
	, {0x44, 0x64, 0x54, 0x4c, 0x44} // 7a z
	, {0x00, 0x08, 0x36, 0x41, 0x00} // 7b {
	, {0x00, 0x00, 0x7f, 0x00, 0x00} // 7c |
	, {0x00, 0x41, 0x36, 0x08, 0x00} // 7d }
	, {0x10, 0x08, 0x08, 0x10, 0x08} // 7e ~
	, {0x78, 0x46, 0x41, 0x46, 0x78} // 7f DEL
};

uint16_t contrast;
uint16_t mode;
int COLS;
int ROWS;	


/*Functions*/
    
void LCD5110_init()
{	
	// configure control pins
	pinMode(SCE, DIGITAL_OUTPUT);
	pinMode(RESET, DIGITAL_OUTPUT);
	pinMode(DC, DIGITAL_OUTPUT);
	pinMode(SDIN, DIGITAL_OUTPUT);
	pinMode(SCLK, DIGITAL_OUTPUT);
	
	// set default display values
	COLS = 84;
	ROWS = 48;
	mode = 0x0C; // set to normal
	contrast = 0xB0;//0xBC;
    
    // this sends the magical commands to the PCD8544
	// reset the LCD to a known state
	digitalWrite(RESET, LOW);
	digitalWrite(RESET, HIGH);

	LCD5110_write(LCD_COMMAND, 0x21); // tell LCD that extended commands follow
	LCD5110_write(LCD_COMMAND, contrast); // set LCD Vop (Contrast): Try 0xB1(good @ 3.3V) or 0xBF if your display is too dark
	LCD5110_write(LCD_COMMAND, 0x04); // set Temp coefficent
	LCD5110_write(LCD_COMMAND, 0x14); // LCD bias mode 1:48: Try 0x13 or 0x14

	LCD5110_write(LCD_COMMAND, 0x20); // we must send 0x20 before modifying the display control mode
	LCD5110_write(LCD_COMMAND, mode); // set display control, normal mode. 0x0D for inverse
}

void LCD5110_setContrast(uint16_t _contrast)
{
	contrast = _contrast;
}

void LCD5110_setDimensions(int _COLS, int _ROWS)
{
	COLS = _COLS;
	ROWS = _ROWS;
}

void LCD5110_invert()
{
	if (mode == 0x0C)
	{
		mode = 0x0D; // change to inverted
	}
	else if (mode == 0x0D)
	{
		mode = 0x0C; // change to normal
	}
}

void LCD5110_gotoXY(int _x, int _y)
{
	LCD5110_write(0, 0x80 | _x);  // column.
	LCD5110_write(0, 0x40 | _y);  // row.  ?
}

// this takes a large array of bits and sends them to the LCD
void LCD5110_bitmap(char _bitmapArray[])
{   int index;
	for (index = 0; index < (COLS * ROWS / 8); index++)
	{
		LCD5110_write(LCD_DATA, _bitmapArray[index]);
	}
}

void LCD5110_progBitmap(char const _bitmapArray[])
{   int index;
	for (index = 0; index < (COLS * ROWS / 8); index++)
	{
		LCD5110_write(LCD_DATA, (uint8_t)_bitmapArray[index]);
	}
}

// This function takes in a character, looks it up in the font table/array
// And writes it to the screen
// Each character is 8 bits tall and 5 bits wide. We pad one blank column of
// pixels on the right side of the character for readability.
void LCD5110_character(char _character)
{
	//LCDWrite(LCD_DATA, 0x00); // blank vertical line padding before character
    int index;
	for (index = 0; index < 5 ; index++)
	{
		LCD5110_write(LCD_DATA, (uint8_t)(ASCII[_character - 0x20][index]));
	}
	//0x20 is the ASCII character for Space (' '). The font table starts with this character

	LCD5110_write(LCD_DATA, 0x00); // blank vertical line padding after character
}

// given a string of characters, one by one is passed to the LCD
void LCD5110_string(char* _characters)
{
	while (*_characters)
	{
		LCD5110_character(*_characters++);
	}
}

// clears the LCD by writing zeros to the entire screen
void LCD5110_clear(void)
{   int index;
	for (index = 0; index < (COLS * ROWS / 8); index++)
	{
		LCD5110_write(LCD_DATA, 0x00);
	}

	LCD5110_gotoXY(0, 0); // after we clear the display, return to the home position
}

// There are two memory banks in the LCD, data/RAM and commands. This
// function sets the DC pin high or low depending, and then sends
// the data byte
void LCD5110_write(uint8_t _data_or_command, uint8_t _data)
{
	digitalWrite(DC, _data_or_command); // tell the LCD that we are writing either to data or a command

	// send the data
	digitalWrite(SCE, LOW);
	LCD5110_shiftOut(_data);
	digitalWrite(SCE, HIGH);
}

void LCD5110_shiftOut(uint8_t _data) //MSBFIRST
{
        digitalWrite(SDIN, _data&0x80);
        digitalWrite(SCLK, HIGH);
        _data<<=1;
        digitalWrite(SCLK, LOW);
        
        digitalWrite(SDIN, _data&0x80);
        digitalWrite(SCLK, HIGH);
        _data<<=1;
        digitalWrite(SCLK, LOW);
        
        digitalWrite(SDIN, _data&0x80);
        digitalWrite(SCLK, HIGH);
        _data<<=1;
        digitalWrite(SCLK, LOW);
        
        digitalWrite(SDIN, _data&0x80);
        digitalWrite(SCLK, HIGH);
        _data<<=1;
        digitalWrite(SCLK, LOW);        

        digitalWrite(SDIN, _data&0x80);
        digitalWrite(SCLK, HIGH);
        _data<<=1;
        digitalWrite(SCLK, LOW);
        
        digitalWrite(SDIN, _data&0x80);
        digitalWrite(SCLK, HIGH);
        _data<<=1;
        digitalWrite(SCLK, LOW);
        
        digitalWrite(SDIN, _data&0x80);
        digitalWrite(SCLK, HIGH);
        _data<<=1;
        digitalWrite(SCLK, LOW);
        
        //digitalWrite(DC, LOW);
        digitalWrite(SDIN, _data&0x80);
        digitalWrite(SCLK, HIGH);
        _data<<=1;
        digitalWrite(SCLK, LOW);
        //digitalWrite(DC, HIGH);        

}