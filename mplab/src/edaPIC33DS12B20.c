    
#include"./../inc/edaPIC33DS12B20.h"

void initDS12B20_1(uint8_t ui8Pin)
{
    pinMode(ui8Pin,ODC_OUTPUT);
    OWTouchReset(ui8Pin);      //Reset
    OWWriteByte(ui8Pin,0xCC);  //Skip Rom
    OWWriteByte(ui8Pin,0x44);  //Start Convert
}


int32_t getTempDS12B20OnCycle_1(uint8_t ui8Pin)
{
    static uint8_t sui8Calls=0;
    static int32_t si32Temp = 0;
    
    if(sui8Calls%20==0)
    {
        OWTouchReset(ui8Pin);      //Reset
        OWWriteByte(ui8Pin,0xCC);  //Skip Rom
        OWWriteByte(ui8Pin,0x44);  //Start Convert
        sui8Calls=0;
    }
    else if(sui8Calls%10==0)
    {
        OWTouchReset(ui8Pin);
        OWWriteByte(ui8Pin,0xCC);  //Skip Rom
        OWWriteByte(ui8Pin,0xBE);  //Read Scratch
        uint8_t LSB = OWReadByte(ui8Pin);
        uint8_t MSB = OWReadByte(ui8Pin);
        int16_t Temp = (((int16_t)MSB<<8) | (int16_t)LSB)<<4;
        si32Temp = (((int32_t)Temp)*125)/32;
    }
    sui8Calls++;
    return si32Temp;
}
void initDS12B20_2(uint8_t ui8Pin)
{
    pinMode(ui8Pin,ODC_OUTPUT);
    OWTouchReset(ui8Pin);      //Reset
    OWWriteByte(ui8Pin,0xCC);  //Skip Rom
    OWWriteByte(ui8Pin,0x44);  //Start Convert
}


int32_t getTempDS12B20OnCycle_2(uint8_t ui8Pin)
{
    static uint8_t sui8Calls=0;
    static int32_t si32Temp = 0;
    
    if(sui8Calls%20==0)
    {
        OWTouchReset(ui8Pin);      //Reset
        OWWriteByte(ui8Pin,0xCC);  //Skip Rom
        OWWriteByte(ui8Pin,0x44);  //Start Convert
        sui8Calls=0;
    }
    else if(sui8Calls%10==0)
    {
        OWTouchReset(ui8Pin);
        OWWriteByte(ui8Pin,0xCC);  //Skip Rom
        OWWriteByte(ui8Pin,0xBE);  //Read Scratch
        uint8_t LSB = OWReadByte(ui8Pin);
        uint8_t MSB = OWReadByte(ui8Pin);
        int16_t Temp = (((int16_t)MSB<<8) | (int16_t)LSB)<<4;
        si32Temp = (((int32_t)Temp)*125)/32;
    }
    sui8Calls++;
    return si32Temp;
}


