#include"./../inc/edaPIC33OneWire.h"

//-----------------------------------------------------------------------------
// Generate a 1-Wire reset, return 1 if no presence detect was found,
// return 0 otherwise.
// (NOTE: Does not handle alarm presence from DS2404/DS1994)
//
int OWTouchReset(uint8_t ui8Pin)
{
        int result;
        __delay32(DELAY_G);//tickDelay(G);
        digitalWrite(ui8Pin,0x00); // Drives DQ low
        __delay32(DELAY_H);//tickDelay(H);
        digitalWrite(ui8Pin,0x01); // Releases the bus
        __delay32(DELAY_I);//tickDelay(I);
        result = digitalRead(ui8Pin); // Sample for presence pulse from slave
        __delay32(DELAY_J);//tickDelay(J); // Complete the reset sequence recovery
        return result; // Return sample presence pulse result
}

//-----------------------------------------------------------------------------
// Send a 1-Wire write bit. Provide 10us recovery time.
//
void OWWriteBit(uint8_t ui8Pin,int bit)
{
        if (bit)
        {
                // Write '1' bit
                digitalWrite(ui8Pin,0x00); // Drives DQ low
                __delay32(DELAY_A);//tickDelay(A);
                digitalWrite(ui8Pin,0x01); // Releases the bus
                __delay32(DELAY_B);//tickDelay(B); // Complete the time slot and 10us recovery
        }
        else
        {
                // Write '0' bit
                digitalWrite(ui8Pin,0x00); // Drives DQ low
                __delay32(DELAY_C);//tickDelay(C);
                digitalWrite(ui8Pin,0x01); // Releases the bus
                __delay32(DELAY_D);//tickDelay(D);
        }
}

//-----------------------------------------------------------------------------
// Read a bit from the 1-Wire bus and return it. Provide 10us recovery time.
//
int OWReadBit(uint8_t ui8Pin)
{
        int result;

        digitalWrite(ui8Pin,0x00); // Drives DQ low
        __delay32(DELAY_A);//tickDelay(A);
        digitalWrite(ui8Pin,0x01); // Releases the bus
        __delay32(DELAY_E);//tickDelay(E);
        result = digitalRead(ui8Pin); // Sample the bit value from the slave
        __delay32(DELAY_F);//tickDelay(F); // Complete the time slot and 10us recovery

        return result;
}

//-----------------------------------------------------------------------------
// Write 1-Wire data byte
//
void OWWriteByte(uint8_t ui8Pin,int data)
{
        int loop;

        // Loop to write each bit in the byte, LS-bit first
        for (loop = 0; loop < 8; loop++)
        {
                OWWriteBit(ui8Pin,data & 0x01);

                // shift the data byte for the next bit
                data >>= 1;
        }
}

//-----------------------------------------------------------------------------
// Read 1-Wire data byte and return it
//
int OWReadByte(uint8_t ui8Pin)
{
        int loop, result=0;

        for (loop = 0; loop < 8; loop++)
        {
                // shift the result to get it ready for the next bit
                result >>= 1;

                // if result is one, then set MS bit
                if (OWReadBit(ui8Pin))
                        result |= 0x80;
        }
        return result;
}

//-----------------------------------------------------------------------------
// Write a 1-Wire data byte and return the sampled result.
//
int OWTouchByte(uint8_t ui8Pin,int data)
{
        int loop, result=0;

        for (loop = 0; loop < 8; loop++)
        {
                // shift the result to get it ready for the next bit
                result >>= 1;

                // If sending a '1' then read a bit else write a '0'
                if (data & 0x01)
                {
                        if (OWReadBit(ui8Pin))
                                result |= 0x80;
                }
                else
                        OWWriteBit(ui8Pin,0);

                // shift the data byte for the next bit
                data >>= 1;
        }
        return result;
}

//-----------------------------------------------------------------------------
// Write a block 1-Wire data bytes and return the sampled result in the same
// buffer.
//
void OWBlock(uint8_t ui8Pin,unsigned char *data, int data_len)
{
        int loop;

        for (loop = 0; loop < data_len; loop++)
        {
                data[loop] = OWTouchByte(ui8Pin,data[loop]);
        }
}

/*
//-----------------------------------------------------------------------------
// Set all devices on 1-Wire to overdrive speed. Return '1' if at least one
// overdrive capable device is detected.
//
int OWOverdriveSkip(unsigned char *data, int data_len)
{
        // set the speed to 'standard'
        SetSpeed(1);

        // reset all devices
        if (OWTouchReset()) // Reset the 1-Wire bus
                return 0; // Return if no devices found

        // overdrive skip command
        OWWriteByte(0x3C);

        // set the speed to 'overdrive'
        SetSpeed(0);

        // do a 1-Wire reset in 'overdrive' and return presence result
        return OWTouchReset();
}
*/
