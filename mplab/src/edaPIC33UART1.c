#include<xc.h>
#include<stdint.h>
#include"./../inc/edaPIC33UART1.h"
char psUart1ReadBuffer[256];

volatile uint8_t ui8Uart1ReadBufferCounter = 0; //global variable for Uart 1 Buffer

volatile uint8_t ui8Uart1NewMessageFlag = 0;

void clrUart1ReadBufferCounter( void )
{
    ui8Uart1ReadBufferCounter = 0;
}

uint8_t getUart1ReadBufferCounter()
{
    return ui8Uart1ReadBufferCounter;
}

uint8_t getUart1NewMessageFlag( void )
{
    return ui8Uart1NewMessageFlag;
}

void clrUart1NewMessageFlag( void )
{
    ui8Uart1NewMessageFlag = 0;
}

void __attribute__ ( (interrupt, no_auto_psv) ) _U1RXInterrupt( void )
{
    while(U1STAbits.URXDA)
        psUart1ReadBuffer[ui8Uart1ReadBufferCounter] = U1RXREG;

    if(psUart1ReadBuffer[ui8Uart1ReadBufferCounter] == 0x0D)
        ui8Uart1NewMessageFlag = 1;
    
    ui8Uart1ReadBufferCounter++;
    
    IFS0bits.U1RXIF = 0;
}

void clrUart1ReadBuffer(void)
{
    uint16_t i;
    for(i=0; i<=256; i++)
        psUart1ReadBuffer[i]='_';
    
    ui8Uart1ReadBufferCounter=0;
    
    clrUart1NewMessageFlag();
}


void initUart1(void)
{
    U1BRG = 1822;//150; //1822 = 9600Baud/s , 150=115200 Baud/s 
    U1MODE = 0x8008; //UARTEN = 1, BRGH = 1 (high baud rate select bit))
    U1STA = 0x0400;
    U1STAbits.URXISEL = 0b00;
    
    IPC7 = 0x4400;        // Mid Range Interrupt Priority level, no urgent reason
    IFS0bits.U1RXIF = 0;  // Clear the Recieve Interrupt Flag
    IEC0bits.U1RXIE = 1;  // Enable Recieve Interrupts
    
    U1MODEbits.UARTEN = 1;// Turn the UART1 peripheral on
    
//    pinMode(DE1, DIGITAL_OUTPUT);
//    digitalWrite(DE1, LOW);
}

void Uart1OutputMapping()
{
    OSCCONL = OSCCON & ~(1<<6);
    
    RPOR9 = 0;
    RPOR6bits.RP87R   = 1;  //0b000001;       //RP87 as U1TX
    RPINR18bits.U1RXR  = 0x77;//119 ; //0b111 0111;    // RPI119 as U1RX = 119;
    
    //RPINR18bits.U1RXR = 100;    //RP100/RF4 as U1RX
    
    
    OSCCONL = (OSCCON | (1<<6));
}


char putCharUart1(char c)
{
    //while(CTS);             //wait for !CTS clear to send
    while(U1STAbits.UTXBF); //wait while Tx buffer full
    U1TXREG = c;
    return c;
}

/* unused function, blocking code!
char getCharUart1(void)
{
    while(!U1STAbits.URXDA);        //wait !!
    
    return U1RXREG;             //red from the recieve buffer
}
*/


void putStrUart1( char *pStr)
{
//    digitalWrite(DE1, HIGH);//enable send data
    while( *pStr) // loop until *s == '\0' the end of the string
        putCharUart1(*pStr++); // send the character and point to the next one
    
    putCharUart1(0x0D); // terminate with a cr
    
    while(!U1STAbits.TRMT); //wait while Transmit Shift Register contains data
    
    //putCharUart1( '\r'); 
    //putCharUart1( '\n');
//    digitalWrite(DE1, LOW);//disable send data
}


char* getNewMsgUart1(char* pMsg)
{
    uint8_t ui8Counter = 0;
    
    for(ui8Counter = 0; psUart1ReadBuffer[ui8Counter] != '\r'; ui8Counter++)
        pMsg[ui8Counter] = psUart1ReadBuffer[ui8Counter];
    
    pMsg[ui8Counter] = '\0';
    
    return pMsg;
}