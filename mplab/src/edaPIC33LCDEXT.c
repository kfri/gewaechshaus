#include "./../inc/edaPIC33LCDEXT.h"
#include "./../inc/edaPIC33Hardware.h"
#include "./../inc/edaPIC33Setup.h"
#include "./../inc/edaPIC33LCD.h"
#include <string.h>

char DataStringExt[80];  

//uint8_t ui8LineUpdateFlag=0; // Bit0: Line1, Bit1: Line2

void initMyLCDExt()
{
    // 15mS delay after Vdd reaches nnVdc before proceeding with LCD initialization
    // not always required and is based on system Vdd rise rate
    uint16_t ui16I=0;
    while(ui16I++<0xFFFF)Nop();

    /* set initial states for the data and control pins */
    DATA_EXT &= 0x00FF; //set RD8-RD15 low
    RW_EXT = 0;                         // R/W state set low
    RS_EXT = 0;                         // RS state set low
    E_EXT = 0;                          // E state set low

    /* set data and control pins to outputs */
    TRISD &= 0x00FF;                    //set RD8-RD15 to output
    RW_TRIS_EXT = 0;                    // RW pin set as output
    RS_TRIS_EXT = 0;                    // RS pin set as output
    E_TRIS_EXT = 0;                     // E pin set as output

    /* 1st LCD initialization sequence */
    DATA_EXT &= 0x00FF;                 //set RD8-RD15 low
    DATA_EXT |= 0x3800;                  //set lcd type: 8-bit,2lines,5x7
    clockLCDExtenable();                       // toggle E signal
    //Delay( Delay_5mS_Cnt );         // 5ms delay
    ui16I=0;
    while(ui16I++<23256)Nop();

    // 2nd LCD initialization sequence
    DATA_EXT &= 0x00FF;
    DATA_EXT |= 0x3800;
    clockLCDExtenable();                        // toggle E signal
    //Delay_Us( Delay200uS_count );   // 200uS delay
    ui16I=0;
    while(ui16I++<4651)Nop();

    // 3rd LCD initialization sequence
    DATA_EXT &= 0x00FF;
    DATA_EXT |= 0x3800;
    clockLCDExtenable();
    //Delay_Us( Delay200uS_count );   // 200uS delay
    ui16I=0;
    while(ui16I++<4651)Nop();

    sendCommandLCDExt( 0x38 );                // function set 8bit, 2lines
    sendCommandLCDExt( 0x0C );                // Display on control, cursor blink off (0x0C), cursor off
    sendCommandLCDExt( 0x06 );                // entry mode set (0x06), increment cursor, no shift 
}

void sendCommandLCDExt(uint8_t ui8data)
{
    DATA_EXT &= 0x00FF;         // prepare RD8 - RD15 , set RD8-RD15 to LOW
    DATA_EXT |= ((uint16_t)ui8data)<<8;        // command byte to lcd , wirte data to RD8-RD15
    RW_EXT = 0;                 // ensure RW is 0
    RS_EXT = 0;
    clockLCDExtenable();
    //Delay( Delay_5mS_Cnt ); // 5ms delay
    uint16_t ui16I=0;
    while(ui16I++<23256)Nop();
}

void sendCommandLCDExtNonBlocking(uint8_t ui8data)
{
    DATA_EXT &= 0x00FF;                         // prepare RD0 - RD7 , set RE7-RE0 to LOW
    DATA_EXT |= ((uint16_t)ui8data)<<8;         // command byte to lcd , wirte data to RE7-RE0
    RW_EXT = 0;                                 // ensure RW is 0
    RS_EXT = 0;
    clockLCDExtenable();
}


void writeDataLCDExt(uint8_t ui8data)
{
    RW_EXT = 0;         // ensure RW is 0
    RS_EXT = 1;         // assert register select to 1
    DATA_EXT &= 0x00FF; // prepare RD0 - RD7 set RE7-RE0 to LOW
    DATA_EXT |= ((uint16_t)ui8data)<<8;   // data byte to lcd,  write data to RE7-RE0
    clockLCDExtenable();
    RS_EXT = 0;         // negate register select to 0
    //Delay_Us( Delay200uS_count );   // 200uS delay
    //Delay_Us( Delay200uS_count );   // 200uS delay
    uint16_t ui16I=0;
    while(ui16I++<4651)Nop();
}

void writeDataLCDExtNonBlocking(uint8_t ui8data)
{
    RW_EXT = 0;         // ensure RW is 0
    RS_EXT = 1;         // assert register select to 1
    DATA_EXT &= 0x00FF; // prepare RD0 - RD7 set RE7-RE0 to LOW
    DATA_EXT |= ((uint16_t)ui8data)<<8;   // data byte to lcd,  write data to RE7-RE0
    clockLCDExtenable();
    RS_EXT = 0;         // negate register select to 0
    //Delay_Us( Delay200uS_count );   // 200uS delay
    //Delay_Us( Delay200uS_count );   // 200uS delay
    //uint16_t ui16I=0;
    //while(ui16I++<4651)Nop();
}

void clockLCDExtenable()
{
    E_EXT = 1;
    Nop();
    Nop();
    Nop();
    Nop();
    E_EXT = 0;          // toggle E signal
    
}

void setDDRAMAddressLCDExt(uint8_t ui8address)
{
    
}

uint8_t readBusyFlagLCDExt()
{
 
    uint8_t ui8ReturnValue=0;
    RS_EXT = 0;
    RW_EXT = 1;

   //set RE7-RE0 to PinMode Digital Input, Pulldown
   ANSELD &= 0x00FF;
   CNEND &= 0x00FF;
   CNPUD &= 0x00FF;
   CNPDD &= 0x00FF;
   TRISD |= 0xFF00;
   CNPDD |= 0xFF00;
   
   clockLCDExtenable();
   
   //read busy flag
   if(PORTD&0x8000) 
   {
       ui8ReturnValue=1;
       
   }
   
    //set RE7-RE0 to PinMode Digital Output
   ANSELD &= 0x00FF;
   CNEND  &= 0x00FF;
   CNPUD  &= 0x00FF;  
   CNPDD  &= 0x00FF;
   TRISD  &= 0x00FF;

    RS_EXT = 0;
    RW_EXT = 0;
   return ui8ReturnValue;
}

void putcLCDExt(char c)
{
    writeDataLCDExt((uint8_t)c);
}

void putsLCDExt(char* pData)
{
    //uint8_t ui8Count=0;
    //for(ui8Count=0; pData[ui8Count]!='\0'; ui8Count++);
    //putcLCD((uint8_t*)pData, ui8Count);
    
    /*uint8_t ui8Count=0;
    while(pData[ui8Count] != '\0')
    {
        writeDataLCD(pData[ui8Count++]);
        if(ui8Count==16)
            line_2();
    }*/
    while(*pData != '\0')
        writeDataLCDExt(*pData++);
}

void putncLCDExt(char* pData, uint8_t ui8n)
{
    //uint8_t ui8Count=0;
    //for(ui8Count=0; pData[ui8Count]!='\0'; ui8Count++);
    //putcLCD((uint8_t*)pData, ui8Count);
    
    uint8_t ui8Count=0;
    while(ui8Count<ui8n)
        writeDataLCDExt(pData[ui8Count++]);
}

void clearLCDExtStorage()
{
    uint8_t i;
    for(i=0;i<80;DataStringExt[i++]=' ');
}

void sendDataToLCDExt()
{
    static uint8_t ui8position = 0;
    static uint8_t ui8setCursor = 0;
    
    //READS BUSY FLAG IF LCD IS READY TO GET CHAR
    if(readBusyFlagLCDExt() == 0)
    {
        //CHECKS THE CURSOR POSITION
        if(ui8setCursor == 1)
        {
            //CHECKS WHICH LINE HAS BEEN WRITTEN AND WHICH IS NEXT
            if(ui8position == 20)
            {
                //WRITES TO SECOND LINE
                //sendCommandLCDExtNonBlocking(0xC0);
                //sendCommandLCDExtNonBlocking(0xC4);
            }
            else if(ui8position == 40)
            {
                //sendCommandLCDExtNonBlocking(0xC4);
            }
            else if(ui8position == 60)
            {
                //sendCommandLCDExtNonBlocking(0xC4);
            }
            else
            {
                //WRITES TO FIRST LINE
                sendCommandLCDExtNonBlocking(0x02);
                ui8position = 0;
            }
            ui8setCursor = 0;
            return;
        }
        
        //ACUTALLY WRITES DATA TO LCD;

        writeDataLCDExtNonBlocking((uint8_t) DataStringExt[ui8position]);
        
        ui8position++;
        
        //IF POSITION IS END OF LINE THE CURSOR HAS TO BE SET TO THE OTHER LINE
        if(ui8position == 20 || ui8position == 40 || ui8position == 60 || ui8position == 80)
        {
            ui8setCursor = 1;
        }
    }
    else
    {
        //do nothing
    }
}

void setLCDExtLine(const char* pStr, uint8_t ui8Line)
{
    //write to storage
    uint8_t ui8counter = 0;
    //*ui8dataPoint = 0;
    char* pStorage;
    if(ui8Line==1)
        pStorage=&DataStringExt[0];
    else if(ui8Line==3) //2 und 3 tauschen --> schneller Workaround! Da zeilen sonst vertauscht?!
        pStorage=&DataStringExt[20];
    else if(ui8Line==2)
        pStorage=&DataStringExt[40];
    else if(ui8Line==4)
        pStorage=&DataStringExt[60];
    else
        return;
        
    //strncpy(*cStorage[32], *ui8dataPoint, 32);
    //differentiate between two lines
    
    //Schreibt die Daten in den Schattenspeicher
    while(pStr[ui8counter] != '\0' && ui8counter < 20)
    {
        pStorage[ui8counter] = pStr[ui8counter];
        ui8counter++;
    }
        
    //Springt hier nur rein, falls das Endezeichen vor dem Ende des Speichers kommt
    while(ui8counter < 20)
    {
        pStorage[ui8counter] = ' ';
        ui8counter++;
    }
}